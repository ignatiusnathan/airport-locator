//
//  MockAirportSearchAPI.swift
//  AirportLocatorTests
//
//  Created by Ignatius Nathan on 11/18/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import RxSwift
@testable import Airport_Locator

final class MockAirportSearchAPI: AirportSearchAPI {
    
    let success: Bool
    
    init(success: Bool) {
        self.success = success
    }
    
    override func request(_ payload: AirportSearchAPIRequestPayload) -> Observable<[Airport]> {
        if success {
            return Observable.just([DataProvider.airport])
            
        } else {
            return Observable.error(ContextualError.networkFailed)
        }
    }
}
