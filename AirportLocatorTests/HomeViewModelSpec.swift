//
//  HomeViewModelSpec.swift
//  AirportLocatorTests
//
//  Created by Ignatius Nathan on 11/18/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import Quick
import Nimble
@testable import Airport_Locator

import MapKit
import RxSwift

class HomeViewModelSpec: QuickSpec {
    
    override func spec() {
        
        var viewModel: HomeViewModel!
        
        describe("Search airports") {
            beforeEach {
                viewModel = HomeViewModel()
            }
            it("success") {
                viewModel.search(coordinate: CLLocationCoordinate2D(),
                                 radius: CLLocationDistance(),
                                 searchAPI: MockAirportSearchAPI(success: true))
                
                waitUntil { (done) in
                    expect(viewModel.airportsRelay.value?.airports.count).to(equal(1))
                    done()
                }
            }
            it("failed") {
                viewModel.search(coordinate: CLLocationCoordinate2D(),
                                 radius: CLLocationDistance(),
                                 searchAPI: MockAirportSearchAPI(success: false))
                
                waitUntil { (done) in
                    expect(viewModel.airportsRelay.value?.airports).to(beNil())
                    done()
                }
            }
        }
        
        let userLocation = CLLocation(latitude: 5.0, longitude: 5.0)
        
        describe("View airport details") {
            beforeEach {
                viewModel = HomeViewModel()
            }
            it("show information") {
                
                let airport = DataProvider.airport
                
                viewModel.viewDetails(airport: airport, userLocation: userLocation)
                
                expect(viewModel.calloutTitle.value).to(equal(airport.name))
                expect(viewModel.calloutIsHidden.value).to(beFalse())
                expect(viewModel.calloutDistance.value).toNot(beNil())
            }
            it("hide information") {
                
                viewModel.viewDetails(airport: nil, userLocation: userLocation)
                
                expect(viewModel.calloutTitle.value).to(beNil())
                expect(viewModel.calloutIsHidden.value).to(beTrue())
                expect(viewModel.calloutDistance.value).to(beNil())
            }
        }
    }
}
