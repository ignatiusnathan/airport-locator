//
//  DataProvider.swift
//  AirportLocatorTests
//
//  Created by Ignatius Nathan on 11/18/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

@testable import Airport_Locator

struct DataProvider {
    
    static let airport = Airport(name: "KLIA2", geometry: Geometry(location: Location(latitude: 1.0, longitude: 1.0)))
}
