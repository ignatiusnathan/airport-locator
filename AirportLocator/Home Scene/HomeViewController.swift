//
//  HomeViewController.swift
//  AirportLocator
//
//  Created by Ignatius Nathan on 11/15/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import UIKit
import MapKit
#if DEBUG
import FLEX
#endif
import RxOptional

final class HomeViewController: ViewController<HomeViewModel> {
    
    @IBOutlet weak var mapView: MKMapView! {
        didSet {
            mapView.delegate = self
        }
    }
    @IBOutlet weak var detailView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    @IBAction func searchDidTap(_ sender: Any) {
        
        searchButton.isHidden = true
        
        let topCenter = mapView.convert(CGPoint(x: mapView.bounds.height/2, y: 0), toCoordinateFrom: mapView)
        let topCenterLocation = CLLocation(latitude: topCenter.latitude, longitude: topCenter.longitude)
        
        let centerLocation = CLLocation(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude)
        
        let radius = topCenterLocation.distance(from: centerLocation)
        
        viewModel.didTapSignal.onNext(
            .search(coordinate: mapView.centerCoordinate,
                    radius: radius))
    }
    
    var viewModel: HomeViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bind()
        #if DEBUG
        let longPressGesture = UILongPressGestureRecognizer(target: self, action:  #selector(debug))
        
        longPressGesture.numberOfTouchesRequired = 1
        longPressGesture.minimumPressDuration = 1
        
        searchButton.addGestureRecognizer(longPressGesture)
        #endif
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return viewModel.calloutIsHidden.value ? .darkContent : .lightContent
    }
    
    #if DEBUG
    @objc private func debug() {
        FLEXManager.shared()?.showExplorer()
    }
    #endif
    
    // MARK:- Private Methods
    private func bind() {
        
        viewModel
            .setRegionSignal
            .subscribeNext { [weak self] (region) in
                self?.mapView.setRegion(region, animated: true)
        }
        .disposed(by: disposeBag)
        
        viewModel
            .airportsRelay
            .asDriver()
            .filterNil()
            .driveNext { [weak self] relay in
                
                guard let self = self else { return }
                
                self.mapView.removeAnnotations(self.mapView.annotations)
                
                for airport in relay.airports {
                    
                    self.mapView.addAnnotation(airport)
                }
        }
        .disposed(by: disposeBag)
        
        viewModel
            .calloutIsHidden
            .asDriver()
            .drive(detailView.rx.isHidden)
            .disposed(by: disposeBag)
        
        viewModel
            .calloutIsHidden
            .asDriver()
            .driveNext({ [weak self] (isHidden) in
                self?.setNeedsStatusBarAppearanceUpdate()
            })
            .disposed(by: disposeBag)
        
        viewModel
            .calloutTitle
            .asDriver()
            .drive(titleLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel
            .calloutDistance
            .asDriver()
            .driveNext({ [weak self] (distance) in
                self?.distanceLabel.isHidden = distance == nil
            })
            .disposed(by: disposeBag)
        
        viewModel
            .calloutDistance
            .asDriver()
            .drive(distanceLabel.rx.text)
            .disposed(by: disposeBag)
        
        viewModel.setupLocation()
    }
}

extension HomeViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, regionWillChangeAnimated animated: Bool) {
        searchButton.isHidden = true
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        searchButton.isHidden = false
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        guard let airport = view.annotation as? Airport else { return }
        
        viewModel.didTapSignal.onNext(.details(airport: airport))
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        
        viewModel.didTapSignal.onNext(.details(airport: nil))
    }
}

// MARK:- Builder
extension HomeViewController {
    
    class func build(_ viewModel: HomeViewModel) -> HomeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: subjectLabel) as! HomeViewController
        vc.viewModel = viewModel
        vc.setPrivateViewModel(viewModel)
        return vc
    }
}
