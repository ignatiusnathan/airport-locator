//
//  AirportSearchAPI.swift
//  AirportLocator
//
//  Created by Ignatius Nathan on 11/15/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import MapKit
import RxSwift

struct AirportSearchAPIRequestPayload: Codable {
    
    let location: String
    let radius: String
    let type: String
    
    init(coordinate: CLLocationCoordinate2D, radius: Int) {
        self.location = String(format: "%@,%@",
                               coordinate.latitude.description, coordinate.longitude.description)
        self.radius = String(radius)
        self.type = "airport"
    }
}

struct AirportSearchAPIResponsePayload: Codable {
    
    let results: [Airport]
    
    let status: String?
}

class AirportSearchAPI: BaseAPI<AirportSearchAPIResponsePayload> {
    
    func request(_ payload: AirportSearchAPIRequestPayload) -> Observable<[Airport]> {
        return request(url: apiService.baseURL,
                       method: .get,
                       requestPayload: payload)
            .flatMap({ (response) -> Observable<[Airport]> in
                if response.results.isEmpty {
                    var errorMessage = "Something went wrong with our server, please try again"
                    if let status = response.status {
                        errorMessage.append("\nStatus: " + status)
                    }
                    return Observable.error(ContextualError.backendError(message: errorMessage))
                }
                return Observable.just(response.results)
            })
    }
}
