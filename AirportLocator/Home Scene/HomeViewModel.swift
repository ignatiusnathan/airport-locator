//
//  HomeViewModel.swift
//  AirportLocator
//
//  Created by Ignatius Nathan on 11/15/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import MapKit
import RxSwift
import RxCoreLocation

enum HomeDidTap {
    case search(
        coordinate: CLLocationCoordinate2D,
        radius: CLLocationDistance
    )
    case details(airport: Airport?)
}

final class HomeViewModel: ViewModel {
    
    private let defaultLocation = CLLocation(latitude: 2.7428174, longitude: 101.6813746)
    private let regionRadius: CLLocationDistance = 50 * 1000 // meters
    
    private let locationManager = CLLocationManager()
    
    // MARK:- Input
    let didTapSignal = PublishSubject<HomeDidTap>()
    
    // MARK:- Output
    let setRegionSignal = PublishSubject<MKCoordinateRegion>()
    
    let airportsRelay = Variable<(source: CLLocationCoordinate2D, airports: [Airport])?>(nil)
    
    let calloutIsHidden = Variable(true)
    let calloutTitle = Variable<String?>(nil)
    let calloutDistance = Variable<String?>(nil)
    
    // MARK:- Class Methods
    
    init() {
        super.init()
        
        locationManager
            .rx.didChangeAuthorization
            .subscribeNext { [weak self] (_, status) in
                switch status {
                case .authorizedAlways, .authorizedWhenInUse:
                    self?.setupLocation()
                default:
                    break
                }
        }
        .disposed(by: disposeBag)
        
        didTapSignal
            .delay(0.1, scheduler: MainScheduler.asyncInstance) // Avoid short calls
            .subscribeNext { [weak self] (didTap) in
                
                guard let self = self else { return }
                
                switch didTap {
                case let .search(coordinate, radius):
                    self.search(coordinate: coordinate, radius: radius)
                    
                case let .details(airport):
                    self.viewDetails(airport: airport, userLocation: self.locationManager.location)
                }
        }
        .disposed(by: disposeBag)
    }
    
    func setupLocation() {
        setDefaultLocation()
        askPermissionIfNeeded()
    }
    
    private func setDefaultLocation() {
        guard let userLocation = locationManager.location else {
            setRegion(location: defaultLocation)
            return
        }
        setRegion(location: userLocation)
    }
    
    private func askPermissionIfNeeded() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways, .authorizedWhenInUse:
            break
        default:
            locationManager.requestWhenInUseAuthorization()
        }
    }
    
    func setRegion(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion(center: location.coordinate,
                                                  latitudinalMeters: regionRadius,
                                                  longitudinalMeters: regionRadius)
        setRegionSignal.onNext(coordinateRegion)
    }
    
    func search(coordinate: CLLocationCoordinate2D,
                radius: CLLocationDistance,
                searchAPI: AirportSearchAPI = AirportSearchAPI()) {
        
        let preferredRadius = Int(min(radius, regionRadius))
        
        searchAPI
            .request(AirportSearchAPIRequestPayload(coordinate: coordinate, radius: preferredRadius))
            .trackActivity(blockingIndicator)
            .addErrorHandler()
            .subscribeNext { [weak self] airports in
                self?.airportsRelay.value = (coordinate, airports)
        }
        .disposed(by: disposeBag)
    }
    
    func viewDetails(airport: Airport?, userLocation: CLLocation?) {
        
        calloutIsHidden.value = airport == nil
        
        calloutTitle.value = airport?.title
        
        calloutDistance.value = getDistance(airport: airport, userLocation: userLocation)
    }
    
    private func getDistance(airport: Airport?, userLocation: CLLocation?) -> String? {
        
        guard let airport = airport else { return nil }
        
        if let userLocation = userLocation {
            
            let airportLocation = CLLocation(latitude: airport.coordinate.latitude, longitude: airport.coordinate.longitude)
            
            let distance = airportLocation.distance(from: userLocation)
            
            let distanceFormatted = { () -> String in
                let kilometer = distance/1000.0
                if kilometer < 1.0 {
                    return String(format: "%d m", Int(distance))
                }
                return String(format: "%0.1f km", kilometer)
            }()
            return String(format: "%@ from you", distanceFormatted)
            
        } else {
            return nil
        }
    }
}
