//
//  NavigationService.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import UIKit

protocol NavigationService {
    func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)?)
    func dismiss(animated flag: Bool, completion: (() -> Void)?)
    func pushViewController(_ viewController: UIViewController, animated: Bool)
    func popViewController(animated: Bool) -> UIViewController?
}

final class NavigationServiceDefault: NavigationService {
    
    func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        guard let vc = UIApplication.shared.keyWindow?.topViewController() else { return }
        let nvc = UINavigationController(rootViewController: viewControllerToPresent)
        vc.present(nvc, animated: flag, completion: completion)
    }
    
    func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        guard let vc = UIApplication.shared.keyWindow?.topViewController() else { return }
        vc.dismiss(animated: flag, completion: completion)
    }
    
    func pushViewController(_ viewController: UIViewController, animated: Bool) {
        guard let nvc = UIApplication.shared.keyWindow?.topViewController()?.navigationController else { return }
        nvc.pushViewController(viewController, animated: animated)
    }
    
    func popViewController(animated: Bool) -> UIViewController? {
        guard let nvc = UIApplication.shared.keyWindow?.topViewController()?.navigationController else { return nil }
        return nvc.popViewController(animated: animated)
    }
}
