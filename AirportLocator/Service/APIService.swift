//
//  APIService.swift
//
//  Created by Ignatius Nathan on 11/15/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import Foundation

protocol APIService {
    var baseURL: URL { get }
    var apiKey: String { get }
}

final class APIServiceDefault: APIService {
    
    let baseURL: URL = URL(string: "https://maps.googleapis.com/maps/api/place/nearbysearch/json")!
    
    var apiKey: String = "AIzaSyDCT3WtvrMKUYURfUONBFkMokIXPtuRBY4"
}
