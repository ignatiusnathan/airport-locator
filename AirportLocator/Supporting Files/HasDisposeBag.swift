//
//  HasDisposeBag.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import Foundation
import RxSwift

protocol HasDisposeBag {
    var disposeBag: DisposeBag { get set }
}
