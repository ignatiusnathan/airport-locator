//
//  ContextualError.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import Foundation

protocol ContextualErrorType: Error {
    var description: String { get }
    var userDescription: String { get }
    var domain: String { get }
    var code: Int { get }
}

extension ContextualError {
    func toNSError() -> NSError {
        let userInfo: [String: Any] = [NSDebugDescriptionErrorKey: self.description,
                                       NSLocalizedDescriptionKey: self.userDescription]
        let error = NSError(domain: domain, code: code, userInfo: userInfo)
        return error
    }
}

enum ContextualError: ContextualErrorType {
    case backendError(message: String)
    case networkFailed
    
    var description: String {
        switch self {
        case let .backendError(message):
            return message
        case .networkFailed:
            return "Failed to start network process"
        }
    }
    
    var code: Int {
        switch self {
        case .backendError:
            return -1
        case .networkFailed:
            return 1
        }
    }
    var domain: String {
        return ""
    }
    var userDescription: String {
        switch self {
        case let .backendError(message):
            return message
        default:
            return "Something went wrong, please try again."
        }
    }
}
