//
//  SharedInstances.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import Foundation
import Swinject
import RxSwift

struct DependancyContainer {
    static let `default`: Container = { () -> Container in
        let container = Container()
        
        let navigationService = NavigationServiceDefault()
        container.register(NavigationService.self) { (_: Resolver) -> NavigationService in
            return navigationService
        }
        let apiService = APIServiceDefault()
        container.register(APIService.self) { (_: Resolver) -> APIService in
            return apiService
        }
        return container
    }()
}

let refreshSignal = PublishSubject<Void>()
