//
//  SubjectLabelable.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import Foundation

protocol SubjectLabelable {
    static var subjectLabel: String { get }
    var subjectLabel: String { get }
}

extension SubjectLabelable {
    static var subjectLabel: String {
        let result = String(describing: Self.self)
        return result
    }
    
    var subjectLabel: String {
        let result = String(describing: type(of: self))
        return result
    }
}

extension NSObject: SubjectLabelable {}
