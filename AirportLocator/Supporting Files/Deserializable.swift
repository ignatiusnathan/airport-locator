//
//  Deserializable.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import Foundation

public protocol Deserializable {
    func deserialize() -> [String: AnyObject]
}
