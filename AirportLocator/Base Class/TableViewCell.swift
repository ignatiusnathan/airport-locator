//
//  TableViewCell.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import RxSwift
import UIKit

class TableViewCell: UITableViewCell, HasDisposeBag {
    var disposeBag = DisposeBag()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nukeDisposeBags()
    }
}
