//
//  ViewModel.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import Foundation
import RxSwift
import SwiftSpinner

class ViewModel: HasDisposeBag {
    
    var disposeBag = DisposeBag()
    
    // Handle view navigations
    let navigationService: NavigationService
    
    // Track reactive processes
    let blockingIndicator = ActivityIndicator()
    
    init(navigationService: NavigationService = DependancyContainer.default.resolve(NavigationService.self)!) {
        self.navigationService = navigationService
        registerActivityIndicator()
    }
    
    private func registerActivityIndicator() {
        blockingIndicator
            .asDriver()
            .drive(UIApplication.shared.rx.isNetworkActivityIndicatorVisible)
            .disposed(by: disposeBag)
        
        blockingIndicator
            .asDriver()
            .driveNext({ [weak self] (isActive) in
                switch isActive {
                case true:
                    if let msg = self?.blockingIndicator.loadingMessage {
                        SwiftSpinner.show(msg)
                    } else {
                        SwiftSpinner.show("Loading...")
                    }
                default:
                    SwiftSpinner.hide()
                }
            })
            .disposed(by: disposeBag)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
