//
//  ViewController.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import UIKit
import RxSwift
import SwiftSpinner

#if DEBUG
import FLEX
#endif

class ViewController<T: ViewModel>: UIViewController, HasDisposeBag {
    
    var disposeBag = DisposeBag()
    
    var _viewModel: T!
    
    var refreshControl: UIRefreshControl?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        #if DEBUG
        let longPressGesture = UILongPressGestureRecognizer(target: self, action:  #selector(longTapped))
        
        longPressGesture.numberOfTouchesRequired = 2
        longPressGesture.minimumPressDuration = 1
        
        self.view.addGestureRecognizer(longPressGesture)
        #endif
    }
    
    func setPrivateViewModel(_ viewModel: T) {
        _viewModel = viewModel
    }
    
    #if DEBUG
    @objc private func longTapped() {
        FLEXManager.shared()?.showExplorer()
    }
    #endif
    
    func addRefreshControl(tableView: UITableView) {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.refreshControl = refreshControl
        self.refreshControl = refreshControl
    }
    
    @objc open func refresh() {
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
