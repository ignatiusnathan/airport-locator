//
//  ActivityIndicator.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2015 Krunoslav Zaher. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

private struct ActivityToken<E>: ObservableConvertibleType, Disposable {
    private let _source: Observable<E>
    private let _dispose: Cancelable
    
    init(source: Observable<E>, disposeAction: @escaping () -> Void) {
        _source = source
        _dispose = Disposables.create(with: disposeAction)
    }
    
    func dispose() {
        _dispose.dispose()
    }
    
    func asObservable() -> Observable<E> {
        return _source
    }
}

/**
 Enables monitoring of sequence computation.
 
 If there is at least one sequence computation in progress, `true` will be sent.
 When all activities complete `false` will be sent.
 */
public class ActivityIndicator: SharedSequenceConvertibleType {
    public typealias E = Bool
    public typealias SharingStrategy = DriverSharingStrategy
    
    private let _lock = NSRecursiveLock()
    private let _variable = Variable(0)
    private let _loading: SharedSequence<SharingStrategy, Bool>
    private var disposeBag = DisposeBag()
    
    let isActive = Variable(false)
    var loadingMessage: String?
    
    public init() {
        _loading = _variable.asDriver()
            .map { $0 > 0 }
            .distinctUntilChanged()
        
        _variable.asObservable()
            .map { $0 > 0 }
            .distinctUntilChanged()
            .bind(to: isActive)
            .disposed(by: disposeBag)
    }
    
    fileprivate func trackActivityOfObservable<O: ObservableConvertibleType>(_ source: O) -> Observable<O.E> {
        return Observable.using({ () -> ActivityToken<O.E> in
            self.increment()
            return ActivityToken(source: source.asObservable(), disposeAction: self.decrement)
        }) { t in
            t.asObservable()
        }
    }
    
    func increment() {
        _lock.lock()
        _variable.value = _variable.value + 1
        _lock.unlock()
    }
    
    func decrement() {
        guard _variable.value > 0 else { return }
        _lock.lock()
        _variable.value = _variable.value - 1
        _lock.unlock()
    }
    
    func reset() {
        _lock.lock()
        _variable.value = 0
        _lock.unlock()
    }
    
    public func asSharedSequence() -> SharedSequence<SharingStrategy, E> {
        return _loading
    }
    
    func trackActivityIndicator(_ activityIndicator: ActivityIndicator) -> Disposable {
        return activityIndicator
            .asDriver()
            .drive(
                onNext: {
                    if $0 {
                        self.increment()
                    } else {
                        self.decrement()
                    }
            }
                , onCompleted: {
                    self.decrement()
            }
                , onDisposed: {
                    self.decrement()
            })
    }
}

extension ObservableConvertibleType {
    public func trackActivity(_ activityIndicator: ActivityIndicator) -> Observable<E> {
        activityIndicator.loadingMessage = nil
        return activityIndicator.trackActivityOfObservable(self)
    }
    public func trackActivity(_ activityIndicator: ActivityIndicator, message: String) -> Observable<E> {
        activityIndicator.loadingMessage = message
        return activityIndicator.trackActivityOfObservable(self)
    }
}
