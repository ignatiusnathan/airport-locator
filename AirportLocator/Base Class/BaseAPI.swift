//
//  BaseAPI.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

class BaseAPI<ResponsePayload: Codable> {
    typealias RequestPayload = Codable
    
    let apiService: APIService
    
    init(apiService: APIService = DependancyContainer.default.resolve(APIService.self)!) {
        self.apiService = apiService
    }
    
    func request(url: URL,
                 method: HTTPMethod,
                 mediaType: String = "application/json",
                 requestPayload: RequestPayload? = nil) -> Observable<ResponsePayload> {
        
        do {
            let baseRequest = try getRequest(url, method: method, mediaType: mediaType, requestPayload: requestPayload)
            
            let result = Observable.create {
                (observer: AnyObserver<ResponsePayload>) -> Disposable in
                
                let request = baseRequest
                    .responseDecodable(completionHandler: { (response: DataResponse<ResponsePayload>) in
                        if let value = response.result.value {
                            observer.onNext(value)
                        }
                        if let error = response.result.error {
                            observer.onError(error)
                        }
                        observer.on(.completed)
                    })
                return Disposables.create(with: { request.cancel() })
            }
            return result
        } catch {
            return Observable.error(error)
        }
    }
    
    func requestArray(url: URL,
                      method: HTTPMethod,
                      mediaType: String = "application/json",
                      requestPayload: RequestPayload? = nil) -> Observable<[ResponsePayload]> {
        
        do {
            let baseRequest = try getRequest(url, method: method, mediaType: mediaType, requestPayload: requestPayload)
            
            let result = Observable.create {
                (observer: AnyObserver<[ResponsePayload]>) -> Disposable in
                
                let request = baseRequest
                    .responseDecodables(completionHandler: { (response: DataResponse<[ResponsePayload]>) in
                        if let value = response.result.value {
                            observer.onNext(value)
                        }
                        if let error = response.result.error {
                            observer.onError(error)
                        }
                        observer.on(.completed)
                    })
                return Disposables.create(with: { request.cancel() })
            }
            return result
        } catch {
            return Observable.error(error)
        }
    }
    
    private func getRequest(_ url: URL,
                            method: HTTPMethod,
                            mediaType: String,
                            requestPayload: RequestPayload?) throws -> DataRequest {
        
        let headers: HTTPHeaders = [
            "Accept": mediaType
        ]
        var params = try requestPayload?.asDictionary()
        if params == nil {
            params = [String: Any]()
        }
        params?["key"] = apiService.apiKey
        
        return Alamofire
            .request(url,
                     method: method,
                     parameters: params,
                     encoding: URLEncoding.default,
                     headers: headers)
            .validate(statusCode: 200..<300)
    }
}

struct NoResponse: Codable {}
