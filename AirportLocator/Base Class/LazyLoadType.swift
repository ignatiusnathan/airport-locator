//
//  LazyLoadType.swift
//
//  Created by Ignatius Nathan on 7/14/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

enum LazyLoadStatus {
    case normal
    case loading
    case completed
}

protocol LazyLoadType {
    
    var currentPage: Int { get }
    var lazyLoadStatus: LazyLoadStatus { get }
}
