//
//  Rx+Extension.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension SharedSequenceConvertibleType where SharingStrategy == DriverSharingStrategy {
    /**
     Subscribes an element handler to an observable sequence.
     
     - parameter onNext: Action to invoke for each element in the observable sequence.
     - returns: Subscription object used to unsubscribe from the observable sequence.
     */
    // @warn_unused_result(message: "http://git.io/rxs.ud")
    func driveNext(_ onNext: @escaping (E) -> Void) -> Disposable {
        return drive(onNext: onNext)
    }
}

extension ObservableType {
    
    /**
     Invokes an action for each Next event in the observable sequence, and propagates all observer messages through the result sequence.
     
     - parameter onNext: Action to invoke for each element in the observable sequence.
     - returns: The source sequence with the side-effecting behavior applied.
     */
    // @warn_unused_result(message:"http://git.io/rxs.uo")
    func doOnNext(_ onNext: @escaping (E) throws -> Void) -> Observable<E> {
        return `do`(onNext: onNext)
    }
    
    /**
     Invokes an action for the Error event in the observable sequence, and propagates all observer messages through the result sequence.
     
     - parameter onError: Action to invoke upon errored termination of the observable sequence.
     - returns: The source sequence with the side-effecting behavior applied.
     */
    // @warn_unused_result(message:"http://git.io/rxs.uo")
    func doOnError(_ onError: @escaping (Swift.Error) throws -> Void) -> Observable<E> {
        return `do`(onError: onError)
    }
    
    /**
     Invokes an action for the Completed event in the observable sequence, and propagates all observer messages through the result sequence.
     
     - parameter onCompleted: Action to invoke upon graceful termination of the observable sequence.
     - returns: The source sequence with the side-effecting behavior applied.
     */
    // @warn_unused_result(message:"http://git.io/rxs.uo")
    func doOnCompleted(_ onCompleted: @escaping () throws -> Void) -> Observable<E> {
        return `do`(onCompleted: onCompleted)
    }
    
    /**
     Subscribes an element handler to an observable sequence.
     
     - parameter onNext: Action to invoke for each element in the observable sequence.
     - returns: Subscription object used to unsubscribe from the observable sequence.
     */
    // @warn_unused_result(message: "http://git.io/rxs.ud")
    func subscribeNext(_ onNext: @escaping (E) -> Void) -> Disposable {
        return subscribe(onNext: onNext)
    }
    
    /**
     Subscribes an error handler to an observable sequence.
     
     - parameter onError: Action to invoke upon errored termination of the observable sequence.
     - returns: Subscription object used to unsubscribe from the observable sequence.
     */
    // @warn_unused_result(message: "http://git.io/rxs.ud")
    func subscribeError(_ onError: @escaping (Swift.Error) -> Void) -> Disposable {
        return subscribe(onError: onError)
    }
    
    /**
     Subscribes a completion handler to an observable sequence.
     
     - parameter onCompleted: Action to invoke upon graceful termination of the observable sequence.
     - returns: Subscription object used to unsubscribe from the observable sequence.
     */
    // @warn_unused_result(message: "http://git.io/rxs.ud")
    func subscribeCompleted(_ onCompleted: @escaping () -> Void) -> Disposable {
        return subscribe(onCompleted: onCompleted)
    }
}

extension ObservableType {
    func addErrorHandler() -> Observable<E> {
        return doOnError({ (error) in
            UIAlertController.alertError(error)
        })
    }
}
