//
//  UIWindow+Extension.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import UIKit

extension UIWindow {
    func topViewController() -> UIViewController? {
        let root = self.rootViewController
        if let presented = root?.presentedViewController {
            return presented
        } else if let nav = root as? UINavigationController {
            return nav.visibleViewController
        } else if let tab = root as? UITabBarController {
            return tab.selectedViewController
        } else {
            return root
        }
    }
}
