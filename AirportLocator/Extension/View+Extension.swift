//
//  View+Extension.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import RxSwift
import UIKit

extension UIView {
    /// Use only to dispose of subscriptions inside resuable views
    func nukeDisposeBags() {
        if var hasDisposeBagView = self as? HasDisposeBag {
            hasDisposeBagView.disposeBag = DisposeBag()
        }
        
        let subViews = self.subviews
        
        guard !subViews.isEmpty else { return }
        
        for subView in subViews {
            subView.nukeDisposeBags()
        }
    }
}
