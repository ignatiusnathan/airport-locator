//
//  DataRequest+Extension.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import Foundation
import Alamofire

extension DataRequest {
    
    private func decodableResponseSerializer<T: Codable>() -> DataResponseSerializer<T> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard
                let data = data,
                let cleanData = data.isEmpty ? "{}".data(using: .utf8) : data
                else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            return Result { try JSONDecoder().decode(T.self, from: cleanData) }
        }
    }
    
    @discardableResult
    func responseDecodable<T: Codable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodableResponseSerializer(), completionHandler: completionHandler)
    }
    
    private func decodablesResponseSerializer<T: Codable>() -> DataResponseSerializer<[T]> {
        return DataResponseSerializer { _, response, data, error in
            guard error == nil else { return .failure(error!) }
            
            guard
                let data = data,
                let cleanData = data.isEmpty ? "[]".data(using: .utf8) : data
                else {
                return .failure(AFError.responseSerializationFailed(reason: .inputDataNil))
            }
            return Result { try JSONDecoder().decode([T].self, from: cleanData) }
        }
    }
    
    @discardableResult
    func responseDecodables<T: Codable>(queue: DispatchQueue? = nil, completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: decodablesResponseSerializer(), completionHandler: completionHandler)
    }
}
