//
//  UIAlertController+Extension.swift
//
//  Created by Ignatius Nathan on 7/12/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

extension UIAlertController {
    
    static func alertError(message: String, actions: [UIAlertAction]? = nil) {
        
        let alert = UIAlertController.init(title: nil, message: message, preferredStyle: .alert)
        
        let actions = { () -> [UIAlertAction] in
            if let actions = actions {
                return actions
            }
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            return [okAction]
        }()
        for action in actions {
            alert.addAction(action)
        }
        guard let vc = UIApplication.shared.keyWindow?.topViewController() else { return }
        vc.present(alert, animated: true, completion: nil)
    }
    
    static func alertError(_ error: Error) {
        if let error = error as? ContextualError {
            alertError(message: error.userDescription)
        } else {
            alertError(message: error.localizedDescription)
        }
    }
}
