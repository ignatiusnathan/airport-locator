//
//  Airport.swift
//  AirportLocator
//
//  Created by Ignatius Nathan on 11/15/19.
//  Copyright © 2019 Ignatius Nathan. All rights reserved.
//

import MapKit

class Airport: NSObject, Codable {
    
    let name: String
    let geometry: Geometry
    
    enum CodingKeys: String, CodingKey {
        case name
        case geometry
    }
    
    init(name: String, geometry: Geometry) {
        self.name = name
        self.geometry = geometry
    }
}

extension Airport: MKAnnotation {
    
    var title: String? {
        return name
    }
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2DMake(geometry.location.latitude, geometry.location.longitude)
    }
}

struct Geometry: Codable {
    let location: Location
}

struct Location: Codable {
    
    let latitude: Double
    let longitude: Double
    
    enum CodingKeys: String, CodingKey {
        case latitude = "lat"
        case longitude = "lng"
    }
}
